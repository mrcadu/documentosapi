import { defineConfig } from 'vite';
import { VitePluginNode } from 'vite-plugin-node';
import Vue from '@vitejs/plugin-vue';
export default defineConfig({
  build: { sourcemap: true },
  server: {
    port: 3000,
  },
  plugins: [
    ...VitePluginNode({
      swcOptions: { sourceMaps: true, inlineSourcesContent: true },
      adapter: 'nest',
      appPath: './src/main.ts',
      exportName: 'viteNodeApp',
      tsCompiler: 'swc',
    }),
    Vue(),
  ],
  optimizeDeps: {
    exclude: [
      '@nestjs/microservices',
      '@nestjs/websockets',
      'cache-manager',
      'class-transformer',
      'class-validator',
      'fastify-swagger',
    ],
  },
});
